import numpy as np

NMI_threshold = 0.6
dist_threshold_1 = 50
dist_threshold = 50
beta = 4*np.pi
num_splits = 1
min_size = 40
time_steps = 500
times_to_run = 10000
wgt_mode = 1
good_vals = [0,1,2,40,50,70,80,90]


#this is set by the adj function, but an inital value is used here
delta_t = 0.5


def norm(x, y):
    return np.sqrt((x[0]-y[0])**2 + (x[1] - y[1])**2)


def wgt_func(M, d_t):
    return np.exp(-M * (d_t/beta))


def prob_func(wgt, source, dest, edges):
    if wgt_mode == 0:
        #print("calcing  1/%d * %lf * (%d/%d)" % (edges, wgt, source.num_cells, dest.num_cells))
        return (1/edges) * wgt * source.num_cells / dest.num_cells
    elif wgt_mode == 1:
        return wgt
