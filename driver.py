import cluster as cl
import numpy as np
import csv
import sys
import networkx as nx
import matplotlib.pyplot as plt
import cluster_parameters as param
import collections as col
import copy

'''takes name of file on command line'''

file_spec = sys.argv[1]

with open(file_spec, newline='') as dest_f:
    data_iter = csv.reader(dest_f, delimiter=',', quotechar='"')
    data = [data for data in data_iter]

data_array = np.asarray(data, dtype=np.int8)
visited = np.zeros(data_array.shape)
k = 0
clusters = col.OrderedDict()
while not visited.all():
    i, j = cl.find_first_of(visited, 0)
    clust = cl.Cluster(data_array[i, j], k)
    cl.BFS_cluster(data_array, visited, i, j, clust)
    if data_array[i,j] in param.good_vals:
        clusters[clust.label] = clust
        k += 1
    if k % 100 == 0:
        print("found cluster %d" % k)


sz = 0

for k in clusters:
    sz += clusters[k].num_cells

print("AVG orig cl sz: %lf" % (sz / len(clusters)))

print("Cluster:                NMI    ")
for key in clusters:
    #cl.cluster_plot(clusters[key], data_array.shape)
    l = clusters[key].get_loc
    clusters[key].calc_NMI()
    print(" %.2f  (%3d, %3d):      %5.3f      " % (clusters[key].label, l[0], l[1], clusters[key].NMI))
    cl.find_min_radius(clusters[key])


cl.save_clusters(clusters, file_spec, 1, data_array.shape)
cl.debug_plot(clusters, data_array.shape)
adj = cl.create_adj_list(clusters)

for i in range(0, len(clusters)):
    for j in range(0, len(clusters)):
        if i == j:
            adj[i, j] = 0
        else:
            if adj[i, j] > param.dist_threshold_1:
                adj[i, j] = 0

x, y = np.shape(adj)
adj1 = np.delete(adj, x-1, 0)
adj2 = np.delete(adj1, y-1, 1)
print("ORIG_ADJ:")
print(adj2)
G1 = nx.from_numpy_matrix(adj2)
fig1, ax1 = plt.subplots()
nx.draw_networkx(G1)
new_c = col.OrderedDict()
new_c[0] = copy.deepcopy(clusters)
for i in range(1, param.num_splits+1):
    new_c[i] = cl.cluster_split(new_c[i-1], i, data_array.shape)
    #cl.clean_graph(new_c[i], data_array)

new_clusters1 = copy.deepcopy(new_c[param.num_splits])

print("Cluster:              CPA      IPQ        NMI    ")
for key in new_clusters1:
    #cl.cluster_plot(new_clusters[key], data_array.shape)
    r = new_clusters1[key].get_loc
    new_clusters1[key].calc_NMI()
    #print(new_clusters1[key].NMI)
    print(" %.4f  (%3d, %3d):     %5.3f   " % (key, r[0], r[1], new_clusters1[key].NMI))


    cl.find_min_radius(new_clusters1[key])

cl.debug_plot(new_clusters1, data_array.shape)
'''
cls = {}
for key in new_clusters1:
    cls[new_clusters1[key].label] = copy.deepcopy(new_clusters1[key])
'''

tmp1 = col.OrderedDict()
i = 0
for key in sorted(new_clusters1):
    tmp1[i] = copy.deepcopy(new_clusters1[key])
    tmp1[i].label = i
    i += 1

new_clusters = copy.deepcopy(tmp1)

sz = 0

for k in new_clusters:
    sz += new_clusters[k].num_cells

print("AVG orig cl sz: %lf" % (sz / len(new_clusters)))

print("Cluster:                 NMI    ")
for key in new_clusters:
    #cl.cluster_plot(new_clusters[key], data_array.shape)
    r = new_clusters[key].get_loc
    new_clusters[key].calc_NMI()

    print(" %3f  (%.3f, %.3f):      %5.3f      " % (key, r[0], r[1], new_clusters[key].NMI))


    cl.find_min_radius(new_clusters[key])

cl.debug_plot(new_clusters, data_array.shape)

cl.save_clusters(new_clusters, file_spec, 2, data_array.shape)
adj = cl.create_adj_list(new_clusters)


for i in range(0, len(new_clusters)):
    for j in range(0, len(new_clusters)):
        if i == j:
            adj[i, j] = 0
        else:
            if adj[i, j] > param.dist_threshold:
                adj[i, j] = 0

x,y = np.shape(adj)
adj1 = np.delete(adj, x-1, 0)
adj2 = np.delete(adj1, y-1, 1)
#G1 = nx.from_numpy_matrix(adj2)
d_t = param.delta_t
w_adj = cl.weight_matrix(adj2, d_t)

flag = cl.check_weights(w_adj, d_t)

while flag != 0 and d_t < 10:
    d_t += 0.01
    w_adj = cl.weight_matrix(adj2, d_t)
    flag = cl.check_weights(w_adj, d_t)

G1 = nx.from_numpy_matrix(w_adj)
fig1, ax1 = plt.subplots()
nx.draw_networkx(G1)
print("ADJ:")
print(adj2)
print("W_ADJ:")
print(w_adj)

clust_iter = new_clusters.copy()
clust_iter[0].present = 1


#cl.spread_plot(clust_iter, data_array.shape)
'''  for one run
for i in range(0, param.time_steps):
    new_iter = cl.monte_carlo(clust_iter, w_adj)
    cl.spread_plot(new_iter, data_array.shape)
    clust_iter = new_iter
'''
for j in range(0, param.times_to_run):
    print("run #%d" % j)
    for key in clust_iter:
        clust_iter[key].present = 0

    clust_iter[0].present = 1
    for i in range(0, param.time_steps):
        new_iter = cl.monte_carlo(clust_iter, w_adj)
        #cl.spread_plot(new_iter, data_array.shape)
        clust_iter = new_iter

cl.visited_plot(clust_iter, data_array.shape)

