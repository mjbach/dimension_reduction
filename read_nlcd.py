import csv
import numpy as np


def round_val(val):
    dig = val//10
    return dig*10


with open("full_hog_data.csv", newline='') as dest_f:
    data_iter = csv.reader(dest_f, delimiter=',', quotechar='"')
    data = [data for data in data_iter]


data_array = np.asarray(data, dtype=np.int8)
new_array = np.zeros(data_array.shape)

for i in range(0, data_array.shape[0]):
    print("line %d" % i)
    for j in range(0, data_array.shape[1]):
        new_array[i][j] = round_val(data_array[i][j])

np.savetxt("rounded_full_hog_data.csv", new_array, fmt='%d', delimiter=',')
