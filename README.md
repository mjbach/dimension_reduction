# dimension_reduction
thesis repository for Matthew B
requires >= numpy 1.9 

driver.py is primary executable
cluster.py contains class definition, methods, and routines. 

driver.py takes one command line argument from "1,2,3", indicating the dataset number you want to run
Will output:
One plot for each cluster identified, showing the cluster against the input data


This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.