import csv
import sys
import numpy as np

'''takes 2 command line arguments, num and mul, where num is the object file and mul is how big you
want to make each cell'''

mul = int(sys.argv[2])

file_spec1 = sys.argv[1]
with open(file_spec1, newline='') as dest_f:
    data_iter = csv.reader(dest_f, delimiter=',', quotechar='"')
    data = [data for data in data_iter]

data1 = np.asarray(data, dtype=np.int8)
data2 = np.zeros((data1.shape[0]*mul, data1.shape[1]*mul), dtype=np.int8)

for i in range(0, data2.shape[0]):
    for j in range(0, data2.shape[1]):
        data2[i][j] = data1[i//mul][j//mul]


file_spec2 = 'large_%s' % sys.argv[1]
with open(file_spec2, 'w+', newline='\n') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerows(data2)

