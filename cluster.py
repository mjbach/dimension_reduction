import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from collections import deque
from collections import OrderedDict
import math as m
import cluster_parameters as param
import copy


mpl.rcParams['figure.max_open_warning'] = -1


class Cluster:
    """
    Class definition for a cluster. The graph will be composed of these linked together
    with the NetworkX package

    'cells' are always tuples of size 2, ie ordered pairs
    """

    def __init__(self, data_val, k):
        self.data_val = data_val
        self.label = k
        self.cells = list()
        self.num_cells = 0
        self.x_vals = 0
        self.y_vals = 0
        self.border = list()
        self.border_size = 0
        self.loc = (-1, -1)
        self.start = (-1, -1)
        self.NMI = -1
        self.adj_list = dict()
        self.min_radius = 0
        self.times_visited = 0
        self.times_split = 0
        self.present = 0


    @property
    def get_loc(self):
        """used to compute the 'location' of the cluster, being the average (x,y) value
        of all the cells"""

        a = self.x_vals / self.num_cells
        b = self.y_vals / self.num_cells
        self.loc = (a, b)
        return self.loc

    def set_start(self, i, j):
        self.start = (i, j)

    def get_cluster(self, a):
        for i in self.cells:
            if i == a:
                return self.get_loc
        return -1, -1

    def get_cells(self):
        return self.cells

    def get_border(self):
        return self.border

    def find_cell(self, a):
        """function to see if a cell is in the cluster"""
        for i in self.cells:
            if i == a:
                return 1
        '''else we have exited'''
        return 0


    def append_cell(self, cell, border):
        """add cell to cluster"""
        self.cells.append(cell)

        '''update the data for cluster'''
        self.num_cells += 1
        self.x_vals += cell[0]
        self.y_vals += cell[1]

        if border:
            self.border_size += 1
            self.border.append(cell)


    def symmetry(self):
        """symmetry calculation for the cluster"""

        l = min(self.border, key=lambda x: x[1])
        r = max(self.border, key=lambda x: x[1])
        u = min(self.border, key=lambda x: x[0])
        d = max(self.border, key=lambda x: x[0])
        m = self.get_loc
        '''this uses the probabilistic definition for skewness
           i.e. sum of the third moments of deviation '''
        lr = ((l[1] - m[1])**3 + (r[1] - m[1])**3)/self.num_cells
        du = ((d[0] - m[0])**3 + (u[0] - m[0])**3)/self.num_cells
        return lr, du


    def calc_CPA(self):
        """returns corrected perimeter area"""
        return (self.border_size * 0.282) / m.sqrt(self.num_cells)


    def calc_IPQ(self):
        """returns iso-perimiteric quotient"""
        return (self.num_cells * 4 * m.pi)/(self.border_size * self.border_size)


    def calc_NMI(self):
        """
        returns the normalized 2nd area moment of the shape (Li, W et al (2013) )
        reference shape is a circle
        """
        ref = (self.num_cells*self.num_cells) / (2*m.pi)
        cen = self.get_loc
        dist = 0
        for c in self.cells:
            dist += ((cen[0] - c[0])**2 + (cen[1] - c[1])**2)

        if dist != 0:
            self.NMI = ref / dist
        else:
            self.NMI = ref

    '''
    def calc_sq_NMI(self):
        """
        returns the normalized 2nd area moment of the shape (Li, W et al (2013) )
        reference shape is a square
        """
        ref = ((self.num_cells)*(self.num_cells))/6
        cen = self.get_loc
        dist = 0
        for c in self.cells:
            dist += ((cen[0] - c[0])**2 + (cen[1] - c[1])**2)

        return ref/dist
'''
    '''

     hex calc is currently bugged
    def calc_hex_NMI(self):
        """
        returns the normalized 2nd area moment of the shape (Li, W et al (2013) )
        reference shape is a circle
        """
        ref = ((self.num_cells)*(self.num_cells) * 20 * m.sqrt(3)) / 432
        cen = self.get_loc
        dist = 0
        for c in self.cells:
            dist += ((cen[0] - c[0])**2 + (cen[1] - c[1])**2)

        return ref / dist
    '''


def BFS_cluster(data, visited, a, b, clust):
    """Starts s BFS at node (i,j)"""
    Q = deque()
    Q.append((a, b))
    clust.set_start(a, b)

    while Q:
        border = False
        i, j = Q.popleft()
        if j >= 0 and 0 <= i < data.shape[0] and j < data.shape[1] and visited[i, j] == 0 and data[i][j] == clust.data_val:
            visited[i, j] = 1
            if i+1 >= data.shape[0] or j+1 >= data.shape[1] or i-1 < 0 or j-1 < 0:
                border = True
            elif data[i+1, j] != clust.data_val or data[i-1,j] != clust.data_val or data[i,j+1] != clust.data_val or data[i,j-1] != clust.data_val:
                border = True

            clust.append_cell((i, j), border)
            Q.append((i+1, j))
            Q.append((i-1, j))
            Q.append((i, j+1))
            Q.append((i, j-1))


def cluster_plot(clust, s):
    data_array = np.zeros(s)
    for i, j in clust.get_cells():
        data_array[i, j] = 2

    for i,j in clust.get_border():
        data_array[i, j] = 1

    fig, ax = plt.subplots()
    heatmap = ax.pcolor(data_array)

    # want a more natural, table-like display
    ax.invert_yaxis()
    ax.xaxis.tick_top()
    plt.ion()
    plt.show()


def find_min_radius(cluster):
    min_dist = 100000000000
    cen = cluster.get_loc
    for b in cluster.border:
        dist = param.norm(cen, b)
        if dist < min_dist:
            min_dist = dist

    cluster.min_radius = min_dist
    return


def find_first_of(arr, val):
    a, b = arr.shape
    for i in range(a):
        for j in range(b):
            if val == arr[i, j]:
                return i, j
    return -1


def create_adj_list(clusters):
    '''
    :param clusters:
    :return:
    '''
    ''' creates the adj list for each node '''
    adj = np.zeros((len(clusters)+1, len(clusters)+1))

    for key1 in clusters:
        for key2 in clusters:
            if key1 != key2:
                dist = param.norm(clusters[key1].get_loc, clusters[key2].get_loc) - (clusters[key1].min_radius + clusters[key2].min_radius)
                '''
                dist = 1/dist**2
                if dist < param.dist_threshold:
                    dist = 0
'''
                #print("dist from (%d, %d) to (%d, %d) = %lf" % (clusters[key1].get_loc[0], clusters[key1].get_loc[1], clusters[key2].get_loc[0], clusters[key2].get_loc[1], dist))
                clusters[key1].adj_list[key2] = dist
                adj[clusters[key1].label, clusters[key2].label] = dist

    return adj


def weight_matrix(mat, d_t):
    a,b = mat.shape
    m = np.zeros(mat.shape)
    for i in range(0, a):
        for j in range(0, b):
            if mat[i,j] != 0:
                m[i,j] = param.wgt_func(mat[i,j], d_t)

    return m

def check_weights(adj, d_t):
    flag = 0
    print("checking p values with d_t = %f:" % d_t)
    for i in range(0, adj.shape[0]):
        #print(sum(adj[i,:]))
        if sum(adj[i,:]) > 1:
            flag += 1

    return flag


def get_cen(cells, num):
    """used to compute the 'location' (centroid) of the cluster, being the average (x,y) value
    of all the cells"""

    y_vals = 0
    x_vals = 0

    for c in cells:
        x_vals += c[0]
        y_vals += c[1]

    a = x_vals / num
    b = y_vals / num
    loc = (a, b)
    return loc


def test_NMI(cells):
    """
    returns the normalized 2nd area moment of the shape (Li, W et al (2013) )
    reference shape is a circle
    """

    num_cells = len(cells)
   # print('num_cells = %d' % num_cells)
    if num_cells == 0:
        return 0
    cen = get_cen(cells, num_cells)
    ref = (num_cells*num_cells) / (2*m.pi)
    dist = 0
    for c in cells:
        dist += ((cen[0] - c[0])**2 + (cen[1] - c[1])**2)
    if dist == 0:
        return ref
    else:
        return ref / dist


def gen_BFS(clust, boundary, axis):
    Q = deque()
    cells = clust.get_cells()
    if axis == 0:
        Q.append((boundary[0][0], boundary[0][1] + 1))
        #print("STARTING BFS AT: (%d, %d)" % (boundary[0][0], boundary[0][1] + 1))
    else:
        Q.append((boundary[1][0]+1, boundary[1][1]))
        #print("STARTING BFS AT: (%d, %d)" % (boundary[1][0], boundary[1][1] + 1))

    #print("ON BOUNDARY:")
    #print(boundary)
    new_cells = []
    visited = {}

    for c in cells:
        visited[c] = 0

    for c in boundary:
        visited[c] = 1

    while Q:
        c = Q.popleft()
        #print("checking cell (%d, %d)" % (c[0], c[1]))
        if c in cells and visited[c] == 0:
            visited[c] = 1
            new_cells.append(c)
            #print("added cell (%d, %d)" % (c[0], c[1]))
            Q.append((c[0]+1, c[1]))
            Q.append((c[0]-1, c[1]))
            Q.append((c[0], c[1]+1))
            Q.append((c[0], c[1]-1))

    return new_cells


def find_end_points(s, axis):

    l = min(s, key=lambda x: x[axis])
    r = max(s, key=lambda x: x[axis])

    ends = []
    s.sort()
    #print("LOOKING AT BORDER SLICE:")
    #print(s)
    for i in range(0, len(s)-1):
        if param.norm(s[i], s[i+1]) > 1:
            ends.append((s[i], s[i+1]))

    #print("ENDS:")
    #print(ends)
   # print("num_ends: %d" % (len(ends)))
    new_ends = []
    for i in range(0, len(ends)):
        if i % 2 == 0:
            new_ends.append(ends[i])

    #print("MODIFIED ENDS")
    #print(new_ends)
    return new_ends


def horz_sweep_2(cluster):

    cut_list = []
    cells = list(cluster.get_cells())
    t_nmi = cluster.NMI
    border = cluster.get_border()
    best_r = []
    best_l = []
    best_i = 0
    best_y = -1
    best_rn = -1
    best_ln = -1
    i_r = 0
    i_l = 0
    l = min(border, key=lambda x: x[1])
    r = max(border, key=lambda x: x[1])

    #print("l/r min/max %d/%d" % (l[1], r[1]))
    for t in range(l[1], r[1]):
        s = [c for c in border if c[1] == t]
        if len(s) < 10:
            ''' e is in end point pairs '''
            e = find_end_points(s, 0)
            cut_list.append(e)

    #del cut_list[0]

    for i in cut_list:

        for j in range(0, len(i)):
            ''' i[j][0][1] = i[j][1][1]'''
            boundary = []
            new_cells = copy.deepcopy(cells)

            for k in range(i[j][0][0], i[j][1][0]+1):
                boundary.append((k, i[j][1][1]))

            #boundary.append((0, i[j][1][1]))
            #print("Boundary:")
           # print(boundary)
            test_area = gen_BFS(cluster, boundary, 0)
            if test_area == new_cells:
                return -1, -1
           # print("Test_Area:")
           # print(test_area)
            for h in test_area:
                del new_cells[new_cells.index(h)]

            #print("num_Orig_cells: %d" % (len(cells)))
            #print("num_l: %d  num_r: %d" % (len(new_cells), len(test_area)))
            #print("Assert: %d = %d" % (len(cells), len(new_cells)+ len(test_area)))

           # print("CELLS:")
           # print(new_cells)
            #print("TEST_AREA")
           # print(test_area)
            r_n = test_NMI(test_area)
            l_n = test_NMI(new_cells)
            i_r = (abs(r_n)/t_nmi)
            i_l = (abs(l_n)/t_nmi)
            #print("  boundary line y = %d, L_NMI = %f, R_NMI = %f" % (i[j][1][1], l_n, r_n))
            #print("    Left increase:  %f" % (abs(l_n)/t_nmi))
            #print("    Right increase: %f" % (abs(r_n)/t_nmi))
            #print("    norm: %f" % np.sqrt(i_l*i_r))

            if np.sqrt(i_l*i_r) > best_i and len(test_area) > param.min_size\
                    and len(new_cells) > param.min_size:
                best_l = new_cells
                best_r = test_area
                best_i = np.sqrt(i_l*i_r)
                best_ln = l_n
                best_rn = r_n
                best_y = i[j][1][1]

                #print("      UPDATED BEST: best_l = %f  best_r = %f" % (l_n, r_n))

    #print("BEST: y = %d, L = %f, R = %f, norm = %f" % (best_y, best_ln, best_rn, best_i))
    return best_l, best_r


def vert_sweep_2(cluster):

    cut_list = []
    cells = list(cluster.get_cells())
    t_nmi = cluster.NMI
    border = cluster.get_border()
    best_r = []
    best_l = []
    best_i = 0
    best_y = -1
    best_rn = -1
    best_ln = -1
    i_r = 0
    i_l = 0
    u = min(border, key=lambda x: x[0])
    d = max(border, key=lambda x: x[0])

    #print("u/d min/max %d/%d" % (u[0], d[0]))
    for t in range(u[0], d[0]):
        s = [c for c in border if c[0] == t]
        ''' e is in end point pairs '''
        if len(s) < 10:
            e = find_end_points(s, 1)
            cut_list.append(e)

    #del cut_list[0]

    for i in cut_list:

        for j in range(0, len(i)):
            ''' i[j][0][1] = i[j][1][1]'''
            boundary = []
            new_cells = copy.deepcopy(cells)

            for k in range(i[j][0][1], i[j][1][1]+1):
                boundary.append((i[j][0][0], k))

            #print(boundary)
            test_area = gen_BFS(cluster, boundary, 1)
            if test_area == new_cells:
                return -1, -1
            #print(test_area)
            for h in test_area:
                del new_cells[new_cells.index(h)]

            #print("num_Orig_cells: %d" % (len(cells)))
            #print("num_l: %d  num_r: %d" % (len(new_cells), len(test_area)))
            #print("Assert: %d = %d" % (len(cells), len(new_cells)+ len(test_area)))

           # print("CELLS:")
           # print(new_cells)
            #print("TEST_AREA")
           # print(test_area)
            r_n = test_NMI(test_area)
            l_n = test_NMI(new_cells)
            i_r = (abs(r_n)/t_nmi)
            i_l = (abs(l_n)/t_nmi)
            #print("  boundary line x = %d, U_NMI = %f, D_NMI = %f" % (i[j][0][0], l_n, r_n))
            #print("    Up increase:  %f" % (abs(l_n)/t_nmi))
            #print("    Down increase: %f" % (abs(r_n)/t_nmi))
            #print("    norm: %f" % np.sqrt(i_l*i_r))

            if np.sqrt(i_l*i_r) > best_i and len(test_area) > param.min_size\
                    and len(new_cells) > param.min_size:
                best_l = new_cells
                best_r = test_area
                best_i = np.sqrt(i_l*i_r)
                best_ln = l_n
                best_rn = r_n
                best_y = i[j][0][0]

                #print("      UPDATED BEST: best_l = %f  best_r = %f" % (l_n, r_n))

    #print("BEST: x = %d, U = %f, D = %f, norm = %f" % (best_y, best_ln, best_rn, best_i))
    return best_l, best_r


def check_border(cluster, cell):
    for c in cluster.border:
        if cell == c:
            return 1

    return 0


def set_border(cluster):

# if the adj cell IS NOT already the cluster and c IS NOT already in the border
    for c in cluster.cells:
        if cluster.find_cell((c[0]+1, c[1])) == 0 and check_border(cluster, c) == 0:
            cluster.border.append(c)
        elif cluster.find_cell((c[0]-1, c[1])) == 0 and check_border(cluster, c) == 0:
            cluster.border.append(c)
        elif cluster.find_cell((c[0], c[1]+1)) == 0 and check_border(cluster, c) == 0:
            cluster.border.append(c)
        elif cluster.find_cell((c[0], c[1]-1)) == 0 and check_border(cluster, c) == 0:
            cluster.border.append(c)

    cluster.border_size = len(cluster.border)
    return


def set_vals(cluster):
    """set the x_vals and y_vals of the cluster"""

    y = 0
    x = 0

    for c in cluster.cells:
        x += c[0]
        y += c[1]

    cluster.x_vals = x
    cluster.y_vals = y

    return


def cluster_split(clusters, num, s):
    '''
        takes each cluster, finds the axis on which to split, tests the split, then makes the split if there is an
        improvement in NMI.
    '''

    new_clusters = {}
    print("%d split" % num)
    for key in clusters:
        test = copy.deepcopy(clusters[key])
        print("  at cluster %f" % test.label)
        if test.NMI < param.NMI_threshold and test.times_split < param.num_splits:

            print("    splitting cluster %f" % test.label)

            ud = vert_sweep_2(test)
            lr = horz_sweep_2(test)

            if ud[0] == -1 and lr[0] == -1:
                print("  cluster %f unsplitable. continuing." % test.label)
                new_clusters[key] = test
                print("    copied %f" % (new_clusters[key].label))
                continue

            avg = len(test.cells)

            #print("size l: %d,  size r: %d, size u: %d, size d: %d" % (len(lr[0]), len(lr[1]), len(ud[0]), len(ud[1])))
            #print("dev l: %f, dev r: %f, dev u: %f, dev d: %f" % (np.sqrt(abs(len(lr[0]) - avg)), np.sqrt(abs(len(lr[1]) - avg)), np.sqrt(abs(len(ud[0]) - avg)), np.sqrt(abs(len(ud[1]) - avg))) )
            s_lr = np.sqrt(abs(len(lr[0]) - avg)) + np.sqrt(abs(len(lr[1]) - avg))
            s_ud = np.sqrt(abs(len(ud[0]) - avg)) + np.sqrt(abs(len(ud[1]) - avg))
            #print("sum lr: %f, sum ud: %f" % (s_lr, s_ud))

            if s_lr > s_ud:
                x = lr[0]
                y = lr[1]
            else:
                x = ud[0]
                y = ud[1]

            if len(x) > param.min_size and len(y) > param.min_size:
                c1 = Cluster(test.data_val, test.label + (3*10**(-num)))
                c1.cells = x
                c2 = Cluster(test.data_val, test.label + (7*10**(-num)))
                c2.cells = y
                c1.num_cells = len(c1.cells)
                c2.num_cells = len(c2.cells)

                set_vals(c1)
                set_vals(c2)

                set_border(c1)
                set_border(c2)

                c1.times_split = test.times_split + 1
                c2.times_split = test.times_split + 1
                new_clusters[c1.label] = c1
                new_clusters[c2.label] = c2
                print("    split into %f and %f" % (c1.label, c2.label))
        else:
            new_clusters[key] = test
            print("    copied %f" % (new_clusters[key].label))

    print("num new_clusters: %d" % len(new_clusters))
    debug_plot(new_clusters, s)
    return new_clusters


def monte_carlo(clusters, adj):
    """
    Monte Carlo type simulator
    :param adj: weighted adj mat for the system - must be symmetric
    :return: clusters after one time step of spread
    """
    ''' remember, here 'key' is a the index of the cluster using the adj list'''
    infected = []

    for key in clusters:
        if clusters[key].present == 1:
            infected.append(key)

    for i in infected:
        next_possible = []
        for j in range(0, len(adj[i, :])):
            if adj[i, j] != 0:
                next_possible.append(j)

        for j in next_possible:
            edges = 0
            for k in range(0, len(adj[j,:])):
                if adj[j,k] != 0:
                    edges += 1

            p = param.prob_func(adj[i, j], clusters[i], clusters[j], edges)
            #print("P for %d:(%d, %d) -> %d:(%d, %d) = %lf" % (i,clusters[i].get_loc[0], clusters[i].get_loc[1],j,clusters[j].get_loc[0], clusters[j].get_loc[1] ,p))

            if np.random.binomial(1, p) == 1:
                clusters[j].times_visited += 1
                if clusters[j].present == 0:
                    clusters[j].present = 1

    return clusters


def spread_plot(clusters, s):
    data_array = np.zeros(s)
    for key in clusters:
        for i, j in clusters[key].get_cells():
            data_array[i, j] = clusters[key].present

    fig, ax = plt.subplots()
    heatmap = ax.pcolor(data_array)

    # want a more natural, table-like display
    ax.invert_yaxis()
    ax.xaxis.tick_top()
    plt.ion()
    plt.show()


def visited_plot(clusters, s):
    data_array = np.zeros(s)
    for key in clusters:
        for i, j in clusters[key].get_cells():
            data_array[i, j] = clusters[key].times_visited

    np.savetxt('visited.csv', data_array, delimiter=',')

    fig, ax = plt.subplots()
    heatmap = ax.pcolor(data_array)

    # want a more natural, table-like display
    ax.invert_yaxis()
    ax.xaxis.tick_top()

    plt.ion()
    plt.show()


def save_clusters(clust, file_spec, option, s):
    if option == 1:
        fn = "initial_clusters_%s" % file_spec
    elif option == 2:
        fn = "final_clusters_%s" % file_spec

    data = np.zeros(s)

    for key in clust:
        for i, j in clust[key].get_cells():
            data[i, j] = clust[key].label

    np.savetxt(fn, data, delimiter=',')


def debug_plot(clust, s):
    data = np.zeros(s)

    for key in clust:
        for i, j in clust[key].get_cells():
            data[i, j] = clust[key].label

    fig, ax = plt.subplots()
    heatmap = ax.pcolor(data)

    ax.invert_yaxis()
    ax.xaxis.tick_top()
    plt.ion()
    plt.show()


def clean_graph(clusters, data_mat):

    checker = np.zeros(data_mat.shape)
    for key in clusters:
        for i, j in clusters[key].get_cells():
            checker[i, j] = 1

    '''this currently only works if there is only one cluster missing'''
    print("Garbage collecting:")
    fig, ax = plt.subplots()
    heatmap = ax.pcolor(checker)

    ax.invert_yaxis()
    ax.xaxis.tick_top()
    plt.ion()
    plt.show()

    x = 0
    c = dict()
    cells = list()
    for i in range(0, data_mat.shape[0]):
        for j in range(0, data_mat.shape[1]):
            if checker[i, j] == 0:
                cells.append((i, j))
                c[x] = (i,j)
                x += 1

    if len(cells) != 0:
        data_val = data_mat[c[0]]

        for a in range(1, len(c)):
            if data_mat[c[a]] != data_val:
                raise ValueError("Different data vals")

        c1 = Cluster(data_val, len(clusters))

        c1.cells = cells
        c1.num_cells = len(c1.cells)
        set_vals(c1)
        set_border(c1)
        clusters[c1.label] = c1






